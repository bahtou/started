'use strict';

angular
  .module('services')
  .factory('studentService', studentService);

studentService.$inject = ['$http', '$q'];

function studentService($http, $q) {

  var create = function(_student, section, callback) {
    var url = '/api/v1/' + section + '/students';
    var deferred = $q.defer();

    $http
      .post(url, { student: _student })
      .success(function(data) {
        deferred.resolve(data.student);
      })
      .error(function(err) {
        deferred.reject(err);
      });

    if (callback) {
      deferred.promise.then(
        function(data) { callback(null, data); },
        function(err) { callback(err); }
      );
    }

    return deferred.promise;
  };

  var destroy = function(_studentId, section, callback) {
    var url = '/api/v1/' + section + '/students/' + _studentId;
    var deferred = $q.defer();

    $http
      .delete(url)
      .success(function(data) {
        deferred.resolve(data);
      })
      .error(function(err) {
        deferred.reject(err);
      });

    if (callback) {
      deferred.promise.then(
        function(data) { callback(null, data); },
        function(err) { callback(err); }
      );
    }

    return deferred.promise;
  };

  var show = function(_studentId, section, callback) {
    var url = '/api/v1/' + section + '/students/' + _studentId;
    var deferred = $q.defer();

    $http
      .get(url)
      .success(function(data) {
        deferred.resolve(data);
      })
      .error(function(err) {
        deferred.reject(err);
      });

    if (callback) {
      deferred.promise.then(
        function(data) { callback(null, data); },
        function(err) { callback(err); }
      );
    }

    return deferred.promise;
  };

  var showAll = function(section, callback) {
    var url = '/api/v1/' + section + '/students/';
    var deferred = $q.defer();

    $http
      .get(url)
      .success(function(data) {
        deferred.resolve(data);
      })
      .error(function(err) {
        deferred.reject(err);
      });

    if (callback) {
      deferred.promise.then(
        function(data) { callback(null, data); },
        function(err) { callback(err); }
      );
    }

    return deferred.promise;
  };

  var update = function(section, _student, callback) {
    var url = '/api/v1/' + section + '/students/' + _student.id;
    var deferred = $q.defer();

    $http
      .put(url, { student: _student })
      .success(function(data) {
        deferred.resolve(data.student);
      })
      .error(function(err) {
        deferred.reject(err);
      });

    if (callback) {
      deferred.promise.then(
        function(data) { callback(null, data); },
        function(err) { callback(err); }
      );
    }

    return deferred.promise;
  };

  return {
    create: create,
    destroy: destroy,
    show: show,
    showAll: showAll,
    update: update
  };
}
