'use strict';

angular
  .module('services', ['LocalStorageModule'])
  .config(localStorageConfig);

localStorageConfig.$inject = ['localStorageServiceProvider'];

function localStorageConfig(localStorageServiceProvider) {
  localStorageServiceProvider.setPrefix('sms');
}
