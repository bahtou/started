'use strict';

angular
  .module('services')
  .factory('storeLocally', storeLocally);

storeLocally.$inject = ['localStorageService'];

function storeLocally(localStorageService) {
  var create = function(student, section, callback) {
    var prefixer = section + '.';
    callback(localStorageService.set(prefixer + student.id, student));
  };

  var destroy = function(studentId, section, callback) {
    var prefixer = section + '.';

    callback(localStorageService.remove(prefixer + studentId));
  };

  var show = function(studentId, section, callback) {
    var prefixer = section + '.';

    callback(localStorageService.get(prefixer + studentId));
  };

  var showAll = function(section, callback) {
    var lsKeys = localStorageService.keys();
    var students = [];

    for (var i=0; i<lsKeys.length; i++) {
      if (lsKeys[i].indexOf(section) > -1) {
        students.push(localStorageService.get(lsKeys[i]));
      }
    }

    callback(students);
  };

  var update = function(section, student, callback) {
    var self = this;

    self.destroy(student.id, section, function(result) {
      if (result !== undefined) {
        console.log('Error updating data locally');
      }

      self.create(student, section, callback);
    });
  };

  return {
    create: create,
    destroy: destroy,
    show: show,
    showAll: showAll,
    update: update
  };
}
