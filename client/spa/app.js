'use strict';

require('angular');
require('angular-ui-router');
require('angular-local-storage');
var _ = require('lodash');

angular
  .module('smsApp', [
    'state.home',
    'services',
    'directives'
  ]);

angular.module('state.home', []);
angular.module('services', []);
angular.module('directives', []);

angular.element(document).ready(function() {
  require('./services/index.js');
  require('./services/storeLocally.js');
  require('./services/studentService.js');

  require('./components/classList/classList.directive.js');

  require('./states/home/index.js');
  require('./states/home/homeController.js');

  angular.bootstrap(document, ['smsApp']);
});
