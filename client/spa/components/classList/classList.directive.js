'use strict';

var componentName = 'classList';
var templateUrl = 'spa/components/classList/classList.html';

angular
  .module('directives')
  .directive(componentName, component);

function component() {
  return {
    templateUrl: templateUrl,
    controller: classListCtrl,
    controllerAs: '$ctrl',
    scope: {
      students: '=',
      gradeSchool: '@'
    },
    bindToController: true
  };
}

classListCtrl.$inject = ['storeLocally', 'studentService', '$scope'];

function classListCtrl(storeLocally, studentService, $scope) {
  var $ctrl = this;
  var section = $ctrl.gradeSchool;

  $ctrl.clickRemove = function(student) {
    $ctrl.deleteForm = student;
  };

  $ctrl.clickEdit = function(student) {
    $ctrl.editForm = student;
  };

  $ctrl.deleleStudent = function(removeStudent, $event) {
    alert('Are you sure?');

    var closeModal = $event.currentTarget.lastElementChild.firstElementChild;
    section = (section === 'K-8') ? 'k8' : 'hs';

    storeLocally.destroy(removeStudent.id, section, function(result) {
      if (result) {
        console.log(result);
      }

      console.log('Student ' + removeStudent.id + ' locally removed successfully');
      studentService.destroy(removeStudent.id, section, function(result) {
        if (result) {
          console.log(result);
        }

        console.log('Student ' + removeStudent.id + ' removed successfully');
        $scope.$emit('student-update');
        closeModal.click();
      });
    });
  };

  $ctrl.editStudent = function(editStudent, $event) {
    var closeModal = $event.currentTarget.lastElementChild.firstElementChild;
    section = (section === 'K-8') ? 'k8' : 'hs';

    storeLocally.update(section, editStudent, function(result) {
      if (!result) {
        console.log('Error locally updating student:', result);
      }

      console.log('Student ' + editStudent.id + ' local update successfull');
      studentService.update(section, editStudent, function(err, updatedStudent) {
        if (!result) {
          console.log('Error updating student:', result);
        }

        console.log('Student ' + updatedStudent.id + ' updated successfully');
        $scope.$emit('student-update');
        closeModal.click();
      });
    });
  };
}
