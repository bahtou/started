'use strict';

angular
  .module('state.home', ['ui.router'])
  .config(routeConfig);

routeConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

function routeConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'spa/layouts/home.html',
      controller: 'HomeController',
      controllerAs: 'ctrl'
    });

  //fall through
  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: true,
    rewriteLinks: false
  });
}
