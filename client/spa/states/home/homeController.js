'use strict';

angular
  .module('state.home')
  .controller('HomeController', IndexController);

IndexController.$inject = ['$timeout', 'storeLocally', 'studentService', '$scope'];

function IndexController($timeout, storeLocally, studentService, $scope) {
  var ctrl = this;

  ctrl.form = {};
  ctrl.gradeSchool = 'K-8';
  ctrl.closed = true;

  getAllStudents();

  ctrl.open = function() {
    ctrl.closed = (!ctrl.closed) ? true : false;
  };

  ctrl.changeGradeSchool = function($event) {
    if ($event.currentTarget.className.indexOf('active') > -1) {
      return;
    }

    swapActiveClass($event.currentTarget.parentElement);
    ctrl.gradeSchool = $event.currentTarget.textContent;
  };

  ctrl.submitForm = function(form, $event) {
    var closeModal = $event.currentTarget.lastElementChild.firstElementChild;
    var section = ctrl.gradeSchool;

    section = (section === 'K-8') ? 'k8' : 'hs';

    //create a random ID for new student
    form.id = _.random(1, 9, true).toFixed(8).split('.')[1];

    storeLocally.create(form, section, function(result) {
      if (!result) {
        console.log('Error saving data locally');
      }

      //showAll again
      getAllStudents();

      //even with local storage error, attempt to send data to server
      console.log('student ' + form.id + ' successfully locally added');
      studentService.create(form, section, function(err, student) {
        if (err) {
          console.log('Error saving data remotely');
        }

        console.log('student ' + student.id + ' successfully added');
        ctrl.form = {};
        closeModal.click();
      });
    });
  };

  ctrl.searchStudent = function() {
    // ctrl.search
  };

  $scope.$on('student-update', function(event) {
    getAllStudents();
  });

  $scope.$watch('ctrl.gradeSchool', function(newValue, oldValue) {
    if (newValue === oldValue) {
      return;
    }

    getAllStudents();
  });

  function getAllStudents() {
    var section = (ctrl.gradeSchool === 'K-8') ? 'k8' : 'hs';

    storeLocally.showAll(section, function(students) {
      ctrl.students = students;
    });
  }

  function swapActiveClass(parentElement) {
    var children = parentElement.children;

    for (var i=0; i < children.length; i++) {
      if (children[i].className.indexOf('active') > -1) {
        children[i].className = 'list-group-item';
      } else {
        children[i].className = 'list-group-item active';
      }
    }
  }
}


