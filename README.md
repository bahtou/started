**School Management System**
=======================
To get started:

    git clone -b master code_challenge.bdl codeChallenge

Then install via **npm**

    npm install

To start the server

    npm start

then go to

    localhost:3000

If unbundling does not work try:

    git clone git@gitlab.com:bahtou/started.git
