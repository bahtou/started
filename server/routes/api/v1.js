'use strict';

var controllers = require('../../controllers');
var apiController = controllers.v1ApiController;

var express = require('express');
var router = express.Router();

  /*
   * API v1
   */
router.route('/k8/students')
  .get(apiController.k8Students.showAll)
  .post(apiController.k8Students.create);

router.route('/k8/students/:studentId')
  .get(apiController.k8Students.show)
  .put(apiController.k8Students.update)
  .delete(apiController.k8Students.destroy);

router.route('/hs/students')
  .get(apiController.hsStudents.showAll)
  .post(apiController.hsStudents.create);

router.route('/hs/students/:studentId')
  .get(apiController.hsStudents.show)
  .put(apiController.hsStudents.update)
  .delete(apiController.hsStudents.destroy);

module.exports = router;
