'use strict';

var v1Router = require('./routes/api/v1');
var controllers = require('./controllers');
var PagesController = controllers.PagesController;

module.exports = function(app) {

  // Load in V2 API
  app.use('/api/v1', v1Router);

  function _renderAngularApp(req, res) {
    PagesController.home(req, res);
  }

  app.get('/', function(req, res) {
    _renderAngularApp(req, res);
  });

  // If we get here, the requested static asset is missing
  function _render404(req, res) {
    res.status(404).send();
  }

  app.all('/api/*', _render404);
};
