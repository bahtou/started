'use strict';

var _ = require('lodash');
var hsStudent = {};

/*
 * Swap array with database hook
 */
var hsStorage = [];

hsStudent.model = {
  'id': {
    'type': 'uuid',
    'required': true
  },
  'gradeLevel': {
    'type': 'Number',
    'required': true
  },
  'firstName': {
    'type': 'String',
    'required': true
  },
  'lastName': {
    'type': 'String',
    'required': true
  },
  'email': {
    'type': 'String',
    'required': true,
    'validate': '_validateEmailAddress'
  }
};

hsStudent.create = function(student, callback) {
  hsStorage.push(student);
  console.log('student ' + student.id + ' successfully added');
  callback(null, student);
};

hsStudent.destory = function(studentId, callback) {
  var removedStudent;
  //find studentId in array and grab index
  var removeIndex = _.find(hsStorage, { id: studentId });

  removedStudent = hsStorage.splice(removeIndex, 1);
  console.log('student ' + studentId + ' successfully removed');
  callback(null, removedStudent);
};

hsStudent.show = function(studentId, callback) {
  //find studentId in array and grab index
  var student = _.find(hsStorage, { id: studentId });
  console.log('student ' + studentId + ' successfully found');
  callback(null, student);
};

hsStudent.showAll = function(studentId, callback) {
  console.log('students successfully found');
  callback(null, hsStorage);
};

hsStudent.update = function(studentId, updateParams, callback) {
  //find studentId in array and grab index
  var studentIndex = _.find(hsStorage, { id: studentId });

  hsStorage.splice(studentIndex, 1);
  hsStorage.push(updateParams);

  console.log('student ' + studentId + ' successfully updated');
  callback(null, updateParams);
};

module.exports = hsStudent;
