'use strict';

var _ = require('lodash');
var k8Student = {};

/*
 * Swap array with database hook
 */
var k8Storage = [];

k8Student.model = {
  'id': {
    'type': 'uuid',
    'required': true
  },
  'gradeLevel': {
    'type': 'Number',
    'required': true
  },
  'firstName': {
    'type': 'String',
    'required': true
  },
  'lastName': {
    'type': 'String',
    'required': true
  },
  'email': {
    'type': 'String',
    'required': true,
    'validate': '_validateEmailAddress'
  }
};

k8Student.create = function(student, callback) {
  k8Storage.push(student);
  console.log('student ' + student.id + ' successfully added');
  console.log('create storage:', k8Storage);
  callback(null, student);
};

k8Student.destory = function(studentId, callback) {
  console.log('destroy storage:', k8Storage);
  var removedStudent;
  //find studentId in array and grab index
  var removeIndex = _.findIndex(k8Storage, function(el) {
    console.log('iterate element:', el);
    return el.id === studentId;
  });

  if (removeIndex > -1) {
    console.log(removeIndex);
    removedStudent = k8Storage.splice(removeIndex, 1);
    console.log('student ' + studentId + ' successfully removed');
  }

  callback(null, studentId);
};

k8Student.show = function(studentId, callback) {
  //find studentId in array and grab index
  var student = _.find(k8Storage, { id: studentId });
  console.log('student ' + studentId + ' successfully found');
  callback(null, student);
};

k8Student.showAll = function(studentId, callback) {
  console.log('students successfully found');
  callback(null, k8Storage);
};

k8Student.update = function(studentId, updateParams, callback) {
  var studentIndex = _.findIndex(k8Storage, function(el) {
    return el.id === studentId;
  });

  if (studentIndex > -1) {
    k8Storage.splice(studentIndex, 1);
    k8Storage.push(updateParams);
    console.log('student ' + studentId + ' successfully updated');
  }

  callback(null, updateParams);
};

module.exports = k8Student;
