'use strict';

var models = require('../../../models');
var k8Student = models.k8Student;

var create = function(req, res) {
  var student = req.body.student;

  k8Student.create(student, function(err, _student) {
    res.json({ student: _student });
  });
};

var destroy = function(req, res) {
  var studentId = req.params.studentId;

  k8Student.destory(studentId, function(err, _student) {
    res.send('success');
  });
};

var show = function(req, res) {
  var studentId = req.params.id;

  k8Student.show(studentId, function(err, _student) {
    res.json({ student: _student });
  });
};

var showAll = function(req, res) {
  k8Student.showAll(function(err, _students) {
    res.json({ students: _students });
  });
};

var update = function(req, res) {
  var studentId = req.params.studentId;
  var updateParams = req.body.student;

  k8Student.update(studentId, updateParams, function(err, _student) {
    res.json({ student: _student });
  });
};

module.exports = {
  create: create,
  destroy: destroy,
  show: show,
  showAll: showAll,
  update: update
};
