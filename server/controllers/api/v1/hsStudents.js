'use strict';

var models = require('../../../models');
var hsStudent = models.hsStudent;

var create = function(req, res) {
  var student = req.body.student;

  hsStudent.create(student, function(err, _student) {
    res.json({ student: _student });
  });
};

var destroy = function(req, res) {
  var studentId = req.params.id;

  hsStudent.destory(studentId, function(err, _student) {
    res.send('success');
  });
};

var show = function(req, res) {
  var studentId = req.params.id;

  hsStudent.show(studentId, function(err, _student) {
    res.json({ student: _student });
  });
};

var showAll = function(req, res) {
  hsStudent.showAll(function(err, _students) {
    res.json({ students: _students });
  });
};

var update = function(req, res) {
  var studentId = req.params.id;
  var updateParams = req.body.student;

  hsStudent.update(studentId, updateParams, function(err, _student) {
    res.json({ student: _student });
  });
};

module.exports = {
  create: create,
  destroy: destroy,
  show: show,
  showAll: showAll,
  update: update
};
