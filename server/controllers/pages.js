'use strict';

/*
 * Pages Controller
 */

var home = function(req, res) {
  res.render('index');
};

module.exports = {
  home: home
};
